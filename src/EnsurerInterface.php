<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ensurer-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ensurer;

use DateTimeInterface;
use InvalidArgumentException;
use Iterator;
use Stringable;

/**
 * EnsurerInterface class file.
 * 
 * This class represents a transformer that ensures that the given values are
 * coerced as the right type.
 * 
 * @author Anastaszor
 */
interface EnsurerInterface extends Stringable
{
	
	/**
	 * Tries to convert the given value to boolean or null. If this is not
	 * possible, an InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return ?bool
	 * @throws InvalidArgumentException
	 */
	public function asBooleanOrNull($value) : ?bool;
	
	/**
	 * Tries to convert the given value as boolean. If this is not possible,
	 * an InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return boolean
	 * @throws InvalidArgumentException
	 */
	public function asBoolean($value) : bool;
	
	/**
	 * Tries to convert the given value as array of booleans. If this is not
	 * possible, an InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return array<integer|string, boolean>
	 * @throws InvalidArgumentException
	 */
	public function asArrayOfBooleans($value) : array;
	
	/**
	 * Tries to convert the given value as array of booleans with integer keys.
	 * If this is not possible, an InvalidArgumentException is thrown.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return array<integer, boolean>
	 * @throws InvalidArgumentException
	 */
	public function asListOfBooleans($value) : array;
	
	/**
	 * Tries to convert the given value as array of booleans with string keys.
	 * If this is not possible, an InvalidArgumentException is thrown.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return array<string, boolean>
	 * @throws InvalidArgumentException
	 */
	public function asMapOfBooleans($value) : array;
	
	/**
	 * Tries to convert the given value as integer or null. If this is not
	 * possible, an InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return ?integer
	 * @throws InvalidArgumentException
	 */
	public function asIntegerOrNull($value) : ?int;
	
	/**
	 * Tries to convert the given value as integer. If this is not possible,
	 * an InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return integer
	 * @throws InvalidArgumentException
	 */
	public function asInteger($value) : int;
	
	/**
	 * Tries to convert the given value as array of integers. If This is not
	 * possible, an InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return array<integer|string, integer>
	 * @throws InvalidArgumentException
	 */
	public function asArrayOfIntegers($value) : array;
	
	/**
	 * Tries to convert the given value as array of integers with integer keys.
	 * If This is not possible, an InvalidArgumentException is thrown.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return array<integer, integer>
	 * @throws InvalidArgumentException
	 */
	public function asListOfIntegers($value) : array;
	
	/**
	 * Tries to convert the given value as array of integers with string keys.
	 * If This is not possible, an InvalidArgumentException is thrown.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return array<string, integer>
	 * @throws InvalidArgumentException
	 */
	public function asMapOfIntegers($value) : array;
	
	/**
	 * Tries to convert the given value as float or null. If this is not
	 * possible, an InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return ?float
	 * @throws InvalidArgumentException
	 */
	public function asFloatOrNull($value) : ?float;
	
	/**
	 * Tries to convert the given value as float. If this is not possible,
	 * an InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return float
	 * @throws InvalidArgumentException
	 */
	public function asFloat($value) : float;
	
	/**
	 * Tries to convert the given value as array of floats. If this is not
	 * possible, an InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return array<integer|string, float>
	 * @throws InvalidArgumentException
	 */
	public function asArrayOfFloats($value) : array;
	
	/**
	 * Tries to convert the given value as array of floats with integer keys.
	 * If this is not possible, an InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return array<integer, float>
	 * @throws InvalidArgumentException
	 */
	public function asListOfFloats($value) : array;
	
	/**
	 * Tries to convert the given value as array of floats with string keys.
	 * If this is not possible, an InvalidArgumentException is thrown.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return array<string, float>
	 * @throws InvalidArgumentException
	 */
	public function asMapOfFloats($value) : array;
	
	/**
	 * Tries to convert the given value as string or null. If this is not
	 * possible, an InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return ?string
	 * @throws InvalidArgumentException
	 */
	public function asStringOrNull($value) : ?string;
	
	/**
	 * Tries to convert the given value as string. If this is not possible,
	 * an InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return string
	 * @throws InvalidArgumentException
	 */
	public function asString($value) : string;
	
	/**
	 * Tries to convert the given value as array of strings. If this is not
	 * possible, an InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return array<integer|string, string>
	 * @throws InvalidArgumentException
	 */
	public function asArrayOfStrings($value) : array;
	
	/**
	 * Tries to convert the given value as array of strings with integer keys.
	 * If this is not possible, an InvalidArgumentException is thrown.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return array<integer, string>
	 * @throws InvalidArgumentException
	 */
	public function asListOfStrings($value) : array;
	
	/**
	 * Tries to convert the given value as array of strings with string keys.
	 * If this is not possible, an InvalidArgumentException is thrown.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return array<integer|string, string>
	 * @throws InvalidArgumentException
	 */
	public function asMapOfStrings($value) : array;
	
	/**
	 * Tries to convert the given value as DateTimeInterface according to the
	 * given formats (php format strings). If this is not possible, an
	 * InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param array<integer|string, string> $formats
	 * @return ?DateTimeInterface
	 * @throws InvalidArgumentException
	 */
	public function asDateTimeOrNull($value, array $formats = []) : ?DateTimeInterface;
	
	/**
	 * Tries to convert the given value as DateTimeInterface according to the
	 * given formats (php format strings). If this is not possible, an
	 * InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param array<integer|string, string> $formats
	 * @return DateTimeInterface
	 * @throws InvalidArgumentException
	 */
	public function asDateTime($value, array $formats = []) : DateTimeInterface;
	
	/**
	 * Tries to convert the given value as array of DateTimeInterface according
	 * to the given formats (php format strings). If this is not possible, an
	 * InvalidArgumentException is thrown.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param array<integer|string, string> $formats
	 * @return array<integer|string, DateTimeInterface>
	 * @throws InvalidArgumentException
	 */
	public function asArrayOfDateTimes($value, array $formats = []) : array;
	
	/**
	 * Tries to convert the given value as array with integer keys of
	 * DateTimeInterface according to the given formats (php format strings).
	 * If this is not possible, an InvalidArgumentException is thrown.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param array<integer|string, string> $formats
	 * @return array<integer, DateTimeInterface>
	 * @throws InvalidArgumentException
	 */
	public function asListOfDateTimes($value, array $formats = []) : array;
	
	/**
	 * Tries to convert the given value as array with string keys of
	 * DateTimeInterface according to the given formats (php format strings).
	 * If this is not possible, an InvalidArgumentException is thrown.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param array<integer|string, string> $formats
	 * @return array<string, DateTimeInterface>
	 * @throws InvalidArgumentException
	 */
	public function asMapOfDateTimes($value, array $formats = []) : array;
	
	/**
	 * Tries to convert the given value as generic array. The inner values are
	 * not checked. If this is not possible, an InvalidArgumentExeption is
	 * thrown.
	 * 
	 * @template T
	 * @param T|array<integer|string, T> $value
	 * @return array<integer|string, T>
	 * @throws InvalidArgumentException
	 */
	public function asArray($value) : array;
	
	/**
	 * Tries to convert the given value as generic array with integer keys.
	 * The inner values are not checked. If this is not possible, an
	 * InvalidArgumentExeption is thrown.
	 * 
	 * @template T
	 * @param T|array<integer|string, T> $value
	 * @return array<integer, T>
	 * @throws InvalidArgumentException
	 */
	public function asList($value) : array;
	
	/**
	 * Tries to convert the given value as generic array with string keys.
	 * The inner values are not checked. If this is not possible, an
	 * InvalidArgumentExeption is thrown.
	 * 
	 * @template T
	 * @param T|array<integer|string, T> $value
	 * @return array<string, T>
	 * @throws InvalidArgumentException
	 */
	public function asMap($value) : array;
	
	/**
	 * Tries to convert the given value as in object of class T or null. If
	 * this is not possible, an InvalidArgumentException is thrown.
	 * 
	 * @template T of object
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param class-string<T> $className
	 * @return ?T
	 * @throws InvalidArgumentException
	 */
	public function asObjectOfOrNull($value, string $className) : ?object;
	
	/**
	 * Tries to convert the given value as an object of class T. If this is not
	 * possible, an InvalidArgumentException is thrown.
	 *
	 * @template T of object
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param class-string<T> $className
	 * @return T
	 * @throws InvalidArgumentException
	 */
	public function asObjectOf($value, string $className) : object;
	
	/**
	 * Tries to convert the given value as array of objects of class T. If this
	 * is not possible, an InvalidArgumentException is thrown.
	 * 
	 * @template T of object
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param class-string<T> $className
	 * @return array<integer|string, T>
	 * @throws InvalidArgumentException
	 */
	public function asArrayOf($value, string $className) : array;
	
	/**
	 * Tries to convert the given value as array with integer keys of objects
	 * of class T. If this is not possible, an InvalidArgumentException is
	 * thrown.
	 *
	 * @template T of object
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param class-string<T> $className
	 * @return array<integer, T>
	 * @throws InvalidArgumentException
	 */
	public function asListOf($value, string $className) : array;
	
	/**
	 * Tries to convert the given value as array with string keys of objects
	 * of class T. If this is not possible, an InvalidArgumentException is
	 * thrown.
	 *
	 * @template T of object
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param class-string<T> $className
	 * @return array<string, T>
	 * @throws InvalidArgumentException
	 */
	public function asMapOf($value, string $className) : array;
	
	/**
	 * Tries to convert the given value as iterator of objects of class T. If
	 * this is not possible, an InvalidArgumentException is thrown.
	 * 
	 * @template T of object
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param class-string<T> $className
	 * @return Iterator<integer|string, T>
	 * @throws InvalidArgumentException
	 */
	public function asIteratorOf($value, string $className) : Iterator;
	
	/**
	 * Tries to convert the given value as iterator of objects of class T with
	 * integer keys. If this is not possible, an InvalidArgumentException is
	 * thrown.
	 *
	 * @template T of object
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param class-string<T> $className
	 * @return Iterator<integer, T>
	 * @throws InvalidArgumentException
	 */
	public function asIntIteratorOf($value, string $className) : Iterator;
	
	/**
	 * Tries to convert the given value as iterator of objects of class T with
	 * string keys. If this is not possible, an InvalidArgumentException is
	 * thrown.
	 *
	 * @template T of object
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param class-string<T> $className
	 * @return Iterator<string, T>
	 * @throws InvalidArgumentException
	 */
	public function asStringIteratorOf($value, string $className) : Iterator;
	
}
